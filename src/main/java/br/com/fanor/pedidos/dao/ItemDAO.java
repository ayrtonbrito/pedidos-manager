package br.com.fanor.pedidos.dao;

import br.com.fanor.pedidos.model.Item;

public class ItemDAO extends GenericDAO<Item> {

	public ItemDAO() {
		super(Item.class);
	}

}
