package br.com.fanor.pedidos.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.fanor.pedidos.dao.ItemDAO;
import br.com.fanor.pedidos.model.Item;



@ManagedBean
@RequestScoped
public class PrincipalBean implements Serializable {

	private static final long serialVersionUID = -6498990685339585132L;

	private String helloWorld = "Hello Senhores";

	private ItemDAO itemDAO;
	
	private Item item;

	@PostConstruct
	public void init() {
		itemDAO = new ItemDAO();
	}

	public String getHelloWorld() {
		return helloWorld;
	}

	public void setHelloWorld(String helloWorld) {
		this.helloWorld = helloWorld;
	}

}
